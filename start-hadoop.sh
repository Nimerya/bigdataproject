#!/bin/bash

HADOOP_HOME="/Users/valentina/Library/hadoop-2.9.2"
INPUT_DIR="/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject/InputsOutputs"

echo "cleaning..."
rm -rf /tmp/hadoop-valentina/

echo "formatting dfs..:"
$HADOOP_HOME/bin/hadoop namenode -format
hdfs dfsadmin -safemode leave


echo "starting dfs..."
$HADOOP_HOME/sbin/start-dfs.sh

echo "starting yarn..."
$HADOOP_HOME/sbin/start-yarn.sh

