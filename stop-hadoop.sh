#!/bin/bash

echo "stopping dfs..."
$HADOOP_HOME/sbin/stop-dfs.sh

echo "stopping yarn..."
$HADOOP_HOME/sbin/stop-yarn.sh

echo "done."