# BigDataProject

The aim of this project is to realise a compression/decompression mechanism to decrease the space occupied by large text files. The entire mechanism is implemented by using a naive intuition based to assign at each word or  punctuation mark a unique ID and then substitute the respective ID to it in the text file. 
The entire compression/decompression mechanism has been developed by the use of Map-Reduce paradigm implemented through Hadoop framework, programmed in java.
The text file, before to be compressed or decompressed, is pre/post-processing by a  python script.  

# Project Organization

The main folder “bigdataproject”, contains all the shall scripts, the README where the purpose of the project is briefly described and the following folder:
- *Compression Algorithm:* contains the code to develop the compression phase.
- *Decompression Algorithm:* contains the code to develop the decompression phase.
- *Docs:* contains a project report explaining the basic idea behind it, the technologies used and a brief explanation of the implementation.
- *Formatting:* contains the python scripts used to perform the Pre-processing and the Post-processing
- *InputsOutputs:* contains two subfolder, one for the input and the output about compression phase and the second for the input and the output about decompression phase.
    - compression folder will be contain the input file to compress, the pre-processed input, the post-processed output and two folders, each one relative to the output of the two rounds of the compression phase.
    - decompression folder will be contain the input file to decompress, the pre-processed input, the post-processed output and the folder of the output of the decompression phase.
