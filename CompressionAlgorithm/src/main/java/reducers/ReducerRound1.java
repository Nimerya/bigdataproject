package reducers;


import com.google.common.collect.Lists;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class ReducerRound1 extends Reducer<Text, Text, Text, IntWritable> {

    /**
     * Reducer function. For each input compute the min id in the value list and emit a pair <word, wordId> where
     * word is the same key of the input and wordId is the computed min id
     *
     * @param key     is a word
     * @param value   is the list of ids given by the different mappers to the word used as key
     * @param context is the context of the job configuration where the reducer will works
     */
    public void reduce(Text key, Iterable<Text> value, Context context) throws IOException, InterruptedException {

        // Emit the pairs of type <word,wordId> where wordId is the min one between all the ids
        context.write(key, new IntWritable(Integer.parseInt(value.iterator().next().toString())));


    }

}
