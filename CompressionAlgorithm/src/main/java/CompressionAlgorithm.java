import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;


import reducers.*;
import mappers.*;

import java.net.URI;


public class CompressionAlgorithm {

    public static void main(String[] args) throws Exception {

        HadoopConfiguration.setup();



        //*********************************** ROUND 1 ***********************************//


        // Inputs to map of round 1
        Path inputRound1 = new Path("compression/compressionInput.txt");

        // Output from reducer of round 1
        Path outputRound1 = new Path("compression/compressionOutputRound1");

        // Create a new configuration
        JobConf round1 = new JobConf();

        // Cleaning the existing folder
        FileSystem fsRound1 = FileSystem.get(round1);
        // True stands for recursively deleting the folder you gave
        fsRound1.delete(outputRound1, true);

        // Creates a new Job
        Job job1 = Job.getInstance(round1, "ROUND 1: Dictionary Builder");

        // Set the Jar by finding where a given class came from
        job1.setJarByClass(CompressionAlgorithm.class);

        // Set the map for round 1
        job1.setMapperClass(MapRound1.class);
        // Set the reducer for Round 1
        job1.setReducerClass(ReducerRound1.class);

        // Set the output types of map
        job1.setMapOutputKeyClass(Text.class);
        job1.setMapOutputValueClass(Text.class);

        // Set the output types of reducer
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(IntWritable.class);

        // Set the path of the input file
        FileInputFormat.setInputPaths(job1, inputRound1);
        //Set the path of the output file
        FileOutputFormat.setOutputPath(job1, outputRound1);

        String separator=" ";
        final Configuration confJob1 = job1.getConfiguration(); //ensure accurate config ref

        confJob1.set("mapreduce.textoutputformat.separator", separator);  //Hadoop v2+ (YARN)
        confJob1.set("mapreduce.output.textoutputformat.separator", separator);
        confJob1.set("mapreduce.output.key.field.separator", separator);
        confJob1.set("mapred.textoutputformat.separatorText", separator); // ?

        //*********************************** ROUND 2 ***********************************//

        // Inputs to map of round 2
        Path inputRound2 = new Path("compression/compressionInput.txt");

        // Output from reducer of round 2
        Path outputRound2 = new Path("compression/compressionOutputRound2");

        // Create a new configuration
        JobConf round2 = new JobConf();

        // Cleaning the existing folder
        FileSystem fsRound2 = FileSystem.get(round2);
        fsRound2.delete(outputRound2, true);

        // Creates a new Job with no particular Cluster and a given Configuration.
        Job job2 = Job.getInstance(round2, "ROUND 2: Compression");

        // Set the Jar by finding where a given class came from
        job2.setJarByClass(CompressionAlgorithm.class);

        // Set the map for round 2
        job2.setMapperClass(MapRound2.class);
        // Set the reducer for round 2
        job2.setReducerClass(ReducerRound2.class);

        // Set the output type of map
        job2.setMapOutputKeyClass(Text.class);
        job2.setMapOutputValueClass(IntWritable.class);

        // Set the output type of reducer
        job2.setOutputKeyClass(IntWritable.class);
        job2.setOutputValueClass(IntWritable.class);

        // Add in the distributed cache the file where is the mapping between all
        // the different terms in the text file and an integer id
        job2.addCacheFile(new URI("/user/valentina/compression/compressionOutputRound1/part-r-00000"+"#dictionary"));

        // Set the path of the input file
        FileInputFormat.setInputPaths(job2, inputRound2);
        //Set the path of the output file
        FileOutputFormat.setOutputPath(job2, outputRound2);

        final Configuration confJob2 = job2.getConfiguration(); //ensure accurate config ref

        confJob2.set("mapreduce.textoutputformat.separator", separator);  //Hadoop v2+ (YARN)
        confJob2.set("mapreduce.output.textoutputformat.separator", separator);
        confJob2.set("mapreduce.output.key.field.separator", separator);
        confJob2.set("mapred.textoutputformat.separatorText", separator); // ?

        // Exit point
        System.exit((job1.waitForCompletion(true) && job2.waitForCompletion(true) ) ? 0 : 1);
        //System.exit((job1.waitForCompletion(true)? 0 : 1 ));
    }
}
