package mappers;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MapRound1 extends Mapper<Object, Text, Text, Text> {

    /**
     * Map function. For each word in the input block insert the word in a list if and only if the word under
     * examination isn't  insert in the list yet. So, in the list there are all DIFFERENT words and for each
     * of them emit a pair <word, wordId> where the key is the word itself and the value is the concatenation
     * of the mapId and an integer number
     *
     * @param key     is empty
     * @param value   is the block of the file
     * @param context is the context of the job configuration where the map will works
     */
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

        // Retrieves the id of the mapper
        int mapId = context.getTaskAttemptID().getId();

        // Set a counter
        int counter = 0;
        // Retrieve all the single words with the respective position in the input block
        String[] wordsPositions = value.toString().split(" ");

        // Declaration of the list that will contain only the words without position
        List<String> words = new ArrayList<>();

        // For each pair word:position, takes only the word
        for (String wordPosition : wordsPositions) {
            words.add(wordPosition.split(":")[0].trim());
        }
        // Create a list with all different words in the block
        Map<String, String> allDifferentWords = new HashMap<>();

        // Fill allDifferentWords list
        for (String word : words) {
            if (!allDifferentWords.containsKey(word) && !(word.equals(" ") || word.equals(""))) {
                counter = counter+1;
                allDifferentWords.put(word, Integer.toString(mapId)+Integer.toString(counter));
                context.write(new Text(word), new Text(allDifferentWords.get(word)));
            }
        }
    }
}