import re
import os
import sys                

source_path = str(sys.argv[1])
destination_path = str(sys.argv[2])

if(len(source_path) == 0 or len(destination_path) == 0):
        print("missing arguments")
        exit(1)

path = "/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject/InputsOutputs/"
open(path + destination_path, 'w').close()


f = open(path + source_path, "r")
f1 = open(path + destination_path, "w")
f2 = open(path + "temp.txt", "a+")

counter = 1

# Adjust the punctuation
content = f.read()
# Detect all that is not letter (capitol or not) or number followed by a single space and replace it with the detected symble,but adding before a single space; 
# replaace all the \n with a space; replace all the \r with nothing
content_temp = re.sub('([^a-zA-Z0-9\s]{1})', r' \1', content, flags = re.M).replace('\n', ' ').replace('\r', '')
# Detect two or more spacecs and replace them with only one space
content_new = re.sub('\ {2,}', ' ', content_temp, flags = re.M)

f2.write(content_new)

f2.seek(0)
f1.seek(0)
# Add the position of each word
for line in f2:
    for word in line.split(" "):
        if (word == "" or word==" "):
                continue
        f1.write(word + ":"+ str(counter) + " ")
        counter += 1

f.close()
f1.close()
f2.close()

os.remove(path + "temp.txt")