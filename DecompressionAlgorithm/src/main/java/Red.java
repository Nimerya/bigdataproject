import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

public class Red extends Reducer<Text, IntWritable,Text,IntWritable> {
    /**
     * Reducer function. It's an identifier function that simply emit what it receives
     * @param key     is a word
     * @param value   is the list of ids given by the different mappers to the word used as key
     * @param context is the context of tßhe job configuration where the reducer will works
     */
    public void reduce(Text key, Iterable<IntWritable> value, Context context) throws IOException, InterruptedException {

        for(IntWritable v: value){
            context.write(key, v);
        }

    }

}
