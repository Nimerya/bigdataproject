import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Map extends Mapper<Object, Text, Text, IntWritable> {
    // Declaration of the dictionary
    private HashMap<String, String> dictionary = new HashMap<>();
    //private static final Logger sLogger = Logger.getLogger(ReducerRound2.class.getName());
    private static final Log sLogger = LogFactory.getLog(Reducer.class);


    /**
     * The setup function is used to retrieves the output data of the previous round saved in the distributed cache.
     *
     * @param context is the context where the reducer will works
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        URI[] cacheFiles = context.getCacheFiles();

        if (cacheFiles != null && cacheFiles.length > 0)
        {
            try
            {
                BufferedReader reader = new BufferedReader(new FileReader("dictionary"));
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] split = line.split(" ");
                    String word = split[0];
                    String id = split[1];
                    sLogger.info(word);
                    sLogger.info(id);
                    dictionary.put(id,word);
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            sLogger.error("no cache file found");
        }
    }
    /**
     * Map function. For each pair <wordId, position> in the input text block, find the id in the dictionary
     * and retrieves the respective word. Then, emit the pair <word,position
     * @param key     is empty
     * @param value   is the block of the file
     * @param context is the context of the job configuration where the map will works
     */
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

        String[] wordIdsPositions = value.toString().split(" ");

        for(String wordIdPosition: wordIdsPositions){
            String[] split=wordIdPosition.split(":");
            if(split.length < 2){
                continue;
            }
            String wordId = split[0];
            String position = split[1];
            String word = dictionary.get(wordId);
            context.write(new Text(word), new IntWritable(Integer.parseInt(position)));
        }
    }

}
