import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.net.URI;

public class DecompressionAlgorithm {

        public static void main(String[] args) throws Exception {

            HadoopConfiguration.setup();

            // Inputs to map
            Path input = new Path( "decompression/decompressionInput.txt");

            // Output from reducer
            Path output = new Path("decompression/decompressionOutput");

            // Create a new configuration
            JobConf conf = new JobConf();

            // Cleaning the existing folder
            FileSystem fsRound1 = FileSystem.get(conf);
            // True stands for recursively deleting the folder you gave
            fsRound1.delete(output, true);

            // Creates a new Job
            Job job = Job.getInstance(conf, "Compression Round");

            // Set the Jar by finding where a given class came from
            job.setJarByClass(DecompressionAlgorithm.class);

            // Set the map for round 2
            job.setMapperClass(Map.class);
            // Set the reducer for round 2
            job.setReducerClass(Red.class);


            // Set the output type of map
            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(IntWritable.class);


            // Set the output type of reducer
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);

            // Add in the distributed cache the file where is the mapping between all
            // the different terms in the text file and an integer id
            job.addCacheFile(new URI("/user/valentina/compression/compressionOutputRound1/part-r-00000"+"#dictionary"));


            // Set the path of the input file
            FileInputFormat.addInputPath(job, input);
            //Set the path of the output file
            FileOutputFormat.setOutputPath(job, output);

            String separator = " ";
            final Configuration confJob1 = job.getConfiguration(); //ensure accurate config ref

            confJob1.set("mapreduce.textoutputformat.separator", separator);  //Hadoop v2+ (YARN)
            confJob1.set("mapreduce.output.textoutputformat.separator", separator);
            confJob1.set("mapreduce.output.key.field.separator", separator);
            confJob1.set("mapred.textoutputformat.separatorText", separator); // ?


            // Exit point
            System.exit(job.waitForCompletion(true) ? 0 : 1);
        }
}
